/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.fortestcode;

import javax.annotation.processing.ProcessingEnvironment;

/**
 *
 * @author Acer
 */
public class MaxSubArray {

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else if (b > a) {
            return b;
        }
        return a;
    }
    // 3
    public static int maxSubFastest(int[] A) {
        int[] M = new int[A.length];
        M[0] = 0;
        int m;
        for (int t = 1; t < A.length; t++) {
            System.out.println(A[t]);
            M[t] = max(0, M[t - 1] + A[t]);
        }
        m = 0;
        for (int t = 1; t < A.length; t++) {
            m = Math.max(m, M[t]);

        }
        return m;
    }

    public static void main(String[] args) {
        int[] A = {0, 1, -97, -54, -3};
        int m = maxSubFastest(A);

        System.out.println(m);

    }

}
