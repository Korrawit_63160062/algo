/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.fortestcode;

/**
 *
 * @author Acer
 */
public class A1_11 {

    public static void main(String[] args) {

        int A[] = {3, 5, 4, 3, 5};

        int result = 0;
        for (int i : A) {
            System.out.println(result+" XOR "+i);
            result = result ^ i;
            System.out.println("Result "+result);
        }

        System.out.println(result);

    }
}
