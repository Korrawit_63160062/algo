/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hotpotato;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class hotPotato {

    private static void printArr(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr.length == 1) {
                System.out.println("[" + arr[i] + "]");
            } else if (i == 0) {
                System.out.print("[" + arr[i] + ", ");
            } else if (i == arr.length - 1) {
                System.out.print(arr[i] + "]");
            } else {
                System.out.print(arr[i] + ", ");
            }
        }
    }

    private static int[] remove(int[] arr, int removeIndex) {
        int[] tempArr = new int[arr.length - 1];
        int count = 0;
        for (int i = 0; i < tempArr.length; i++) {
            if (i == removeIndex) {
                count++;
                tempArr[i] = arr[i + count];
            } else {
                tempArr[i] = arr[i + count];
            }
        }
        return tempArr;
    }

    ;
    private static void hotPotato(int[] children, int K, int potato) {
        if (children.length == 1) {
            System.out.println("Winner " + children[0]);
            return;
        }
        potato = ((potato + K) % children.length);
        System.out.println("Now potato is on player " + children[potato] + " and this player will leave the game");
        children = remove(children, potato);
        printArr(children);
        System.out.println("");
        hotPotato(children, K, potato);
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Input N : ");
        int N = kb.nextInt();
        int[] children = new int[N];
        System.out.println("Input K : ");
        int K = kb.nextInt();
        int potato = 0;

        for (int i = 0; i < N; i++) {
            children[i] = i + 1;
        }

        printArr(children);
        System.out.println("");
        hotPotato(children, K, potato);

    }

}
